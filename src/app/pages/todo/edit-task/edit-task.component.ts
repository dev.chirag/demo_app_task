import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
import { TaskManagerService } from "../services/task-manager.service";
@Component({
  selector: "ngx-edit-task",
  templateUrl: "./edit-task.component.html",
  styleUrls: ["./edit-task.component.scss"],
})
export class EditTaskComponent implements OnInit {
  task: string;
  taskType;
  id;
  showError: boolean = false;

  constructor(
    private TaskManager: TaskManagerService,
    private router: Router,
    private currentRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    let data = this.currentRoute.snapshot.paramMap.get("id");

    this.id = parseInt(data);
    console.log(this.id);

    this.getTask(this.id);
  }

  getTask(id) {
    for (let i = 0; i < this.TaskManager.tasksList.length; i++) {
      if (this.TaskManager.tasksList[i].id == id) {
        this.task = this.TaskManager.tasksList[i].task;
        this.taskType = this.TaskManager.tasksList[i].taskType;
      }
    }
  }

  submit() {
    if (
      this.task !== "" &&
      this.task !== undefined &&
      this.task !== null &&
      this.taskType !== "" &&
      this.taskType !== undefined &&
      this.taskType !== null
    ) {
      for (let i = 0; i < this.TaskManager.tasksList.length; i++) {
        if (this.TaskManager.tasksList[i].id == this.id) {
          this.TaskManager.tasksList[i].task = this.task;
          this.TaskManager.tasksList[i].taskType = this.taskType;
        }
      }

      this.router.navigate(["/task-list"]);
    } else {
      this.showError = true;
    }
  }
}
