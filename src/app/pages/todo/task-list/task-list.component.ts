import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { TaskManagerService } from "../services/task-manager.service";

@Component({
  selector: "ngx-task-list",
  templateUrl: "./task-list.component.html",
  styleUrls: ["./task-list.component.scss"],
})
export class TaskListComponent implements OnInit {
  tasksList;

  constructor(
    private TaskManager: TaskManagerService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getTaskList();
  }

  getTaskList() {
    this.tasksList = this.TaskManager.tasksList;
  }

  completeTask(task) {
    // let item = this.TaskManager.tasksList.filter((item) => item === task);

    for (let i = 0; i < this.TaskManager.tasksList.length; i++) {
      if (this.TaskManager.tasksList[i] === task) {
        this.TaskManager.tasksList[i].status = true;
        this.getTaskList();
      }
    }
  }

  deleteTask(task) {
    for (let i = 0; i < this.TaskManager.tasksList.length; i++) {
      if (this.TaskManager.tasksList[i] === task) {
        this.TaskManager.tasksList.splice(i, 1);
        this.getTaskList();
      }
    }
  }

  editTask(task) {
    console.log(task);
    this.router.navigateByUrl("/pages/todo/edit-task/" + task.id);
  }
}
