import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { TodoRoutingModule } from "./todo-routing.module";
import { TaskListComponent } from "./task-list/task-list.component";
import { AddTaskComponent } from "./add-task/add-task.component";
import { FormsModule } from "@angular/forms";
import { EditTaskComponent } from './edit-task/edit-task.component';
import { SettingsComponent } from './settings/settings.component';

@NgModule({
  declarations: [TaskListComponent, AddTaskComponent, EditTaskComponent, SettingsComponent],
  imports: [CommonModule, TodoRoutingModule, FormsModule],
})
export class TodoModule {}
